import React from 'react';
//import background from "./../images/backgound.jpg";

/*react-bootstrap component*/
import Container from 'react-bootstrap/Container'

/*components*/
import Banner from './../components/Banner';
import Highlights from './../components/Highlights';

export default function Home(){

	return(
		<Container fluid style={{background: "black"}}>
			<Banner/> 
			<Highlights/>
		</Container>
	)
}