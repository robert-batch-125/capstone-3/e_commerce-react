import React, {useState, useEffect, useContext} from 'react';
/*react-bootstrap component*/
import {Container} from 'react-bootstrap'

/*components*/
//import Product from './../components/Product';
import AdminView from './../components/AdminView.js';
import UserView from './../components/UserView.js';


/*context*/
import UserContext from './../UserContext';

export default function Products(){

	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext);

	const fetchData = () => {
		let token = localStorage.getItem('token')

		fetch('https://protected-island-01572.herokuapp.com/api/products/all',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)
			setProducts(result)
		})
	}

	useEffect( () => {
		fetchData()
	}, [])


	return(
		<Container className="p-4">
			{ (user.isAdmin === true) ?
					<AdminView productData={products} fetchData={fetchData}/>
				:
					<UserView productData={products} />
			}
		</Container>
	)
}
