import React, {useContext, useEffect, useState} from 'react';

import UserContext from './../UserContext';

import {Link, useParams, useHistory} from 'react-router-dom';

import {Container, Card, Button} from 'react-bootstrap';

import Swal from 'sweetalert2';



export default function SpecificProduct(){

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	const {user} = useContext(UserContext);

	const {productId} = useParams();

	let token = localStorage.getItem('token');

	let history = useHistory();

	useEffect( () => {
		fetch(`https://protected-island-01572.herokuapp.com/api/products/${productId}`,
			{
				method: "GET",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			}
		)
		.then(result => result.json())
		.then(result => {
			console.log(result)

			setName(result.name);
			setDescription(result.description);
			setPrice(result.price);
		})
	}, [productId, token])

	const addToCart = () => {
		fetch(`https://protected-island-01572.herokuapp.com/api/users/checkout`, 
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					productId: productId
					
				})
			}
		)
		.then(result => result.json())
		.then(result => {
			console.log(result)

			if(result === true){

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Order Placed Successfully" 
				})

				history.push('/products');
			} else {
				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "Please try again" 
				})
			}
		})
	}

	return(
		<Container className="my-5">
			<Card>
				<Card.Header>
					<h4>
						{/*product name*/}
						{name}
					</h4>
				</Card.Header>
				<Card.Body>
					<Card.Text>
						{/*product description*/}
						{description}
					</Card.Text>
					<h6>
						Price: Php 
						{/*product price*/}
						<span className="mx-2">{price}</span>
					</h6>
				</Card.Body>
				<Card.Footer>
					{
						(user.id !== null) ?
								<Button variant="primary" 
								onClick={ () => addToCart() }

								> Add to Cart</Button>
							:
								<Link className="btn btn-danger" to="/login">Login to Order</Link>
					}
				</Card.Footer>
			</Card>
		</Container>
	)
}