import React, {Fragment, useContext} from 'react';
import { Link, NavLink, useHistory } from 'react-router-dom';

import image from './../images/e-basket.png';

/*context*/
import UserContext from './../UserContext';

/*react boostrap*/
import {Navbar, Nav, Container, Row} from 'react-bootstrap';

/*app navbar*/
export default function AppNavbar(){
  // console.log(props)

  // destructure context object
  const {user, unsetUser} = useContext(UserContext);

  //useHistory is a react-router-dom hook
  let history = useHistory();


  const logout = () => {
    unsetUser();
    history.push('/login');
  }

  let rightNav = (user.id !== null) ? 
        (user.isAdmin === true) ?
          <Fragment>
            <Nav.Link as={NavLink} to="/addProduct">Add Product</Nav.Link>
            <Nav.Link onClick={logout}>Logout</Nav.Link>
          </Fragment>
        :
          <Fragment>
            <Nav.Link onClick={logout}>Logout</Nav.Link>
          </Fragment>
    :
      (
        <Fragment>
            <Nav.Link as={NavLink} to="/register" className="nav-link btn btn-danger text-white">Register</Nav.Link>
            <Nav.Link as={NavLink} to="/login" className="nav-link btn btn-primary text-white">Log In</Nav.Link>
          </Fragment>

      )

  return (

    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" sticky="top">  
        <Navbar.Brand as={Link} to="/">
          <img src={image} width="40" height="40" className="d-inline-block align-top responsive-image"/>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav" >
        <Container className="justify-content-end">
            <Row>
                <Nav>
                  <Nav.Link as={NavLink} to="/products">Products</Nav.Link>    
                  {rightNav}      
                </Nav>
            </Row>
        </Container>
        </Navbar.Collapse>      
    </Navbar>
  )
}