import React from 'react';

import {Container, Row, Col, Jumbotron} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ErrorPage(){

	return(
		<Container fluid className="text-center">
			<Row>
				<Col className="px-0">
					<Jumbotron  className="px-3">	
						<h1>Oops!</h1>
					 	<h2>404 - Page Not Found.</h2>
					 	<p>The requested URL/ badpage was not found on this server.</p>
					 	<p>Click here to return <Link to="/">Home Page</Link></p>
					</Jumbotron>
				</Col>				
			</Row>
		</Container>
	)
}