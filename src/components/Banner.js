import React from 'react';
//import image from './../images/e-basket.png';
import {Link} from 'react-router-dom';
import background from "./../images/backgound.jpg";

/*react-bootstrap components*/
import {
	Container,
	Row,
	Col,
	Jumbotron,
	Button
} from 'react-bootstrap';

export default function Banner(){

	return(

		<Container fluid>
			<Row>
				<Col className="py-3">
					
					<Jumbotron fluid className="px-3" style={{ backgroundImage: `url(${background})` }}>
						{/*<div style={{textAlign: "center"}}>
							<img src={image} className="responsive-image"/>
						</div>*/}
						<div style={{textAlign: "center", color: "red"}}>
							<h1>E-BASKET Bike Shop</h1>
							<h6>Everyone, Everywhere can buy now.</h6>
							<h6>“Life is like riding a bicycle. To keep your balance you must keep moving”</h6>
							<Button as={Link} to="/login" variant="warning">Order Now</Button>
						</div>  
					</Jumbotron>
				</Col>
			</Row>
		</Container>
	)
}