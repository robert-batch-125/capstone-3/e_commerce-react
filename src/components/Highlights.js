import React from 'react';
import {Link} from 'react-router-dom';
import bmx from './../images/bmx.jpg';
import rb from './../images/cervelo.jpg';
import mtb from './../images/mou.jpg';


/*react-boostrap components*/
import {
	Row,
	Col,
	Card,
	Button
} from 'react-bootstrap'

export default function Highlights(){

	return(

		<Row className="px-3">
			<Col xs={12} md={4}>
				<Card style={{background: "darkgray"}}>
					<img src={rb} className="responsive-image"/>
					<Card.Body style={{color: "white"}}>
						<Card.Title style={{textAlign: "center"}}>Road Bikes</Card.Title>
						<Card.Text >
							The term road bicycle (RB) is used to describe bicycles built for traveling at speed on paved roads. Some sources use the term to mean racing bicycle. Other sources specifically exclude racing bicycles from the definition, using the term to mean a bicycle of a similar style but built more for endurance and less the fast bursts of speed desired in a racing bicycle; as such, they usually have more gear combinations and fewer hi-tech racing features. Certain of these bicycles have been referred to as 'sportive' bicycles to distinguish them from racing bicycles.
						</Card.Text>
				    	<Button as={Link} to="/products" variant="warning" style={{color: "brown"}}>Go to product list</Button>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card style={{background: "darkgray"}}>
					<img src={mtb} className="responsive-image"/>
					<Card.Body style={{color: "white"}}>
						<Card.Title style={{textAlign: "center"}}>Mountain Bikes</Card.Title>
						<Card.Text>
					      A mountain bike (MTB) or mountain bicycle is a bicycle designed for off-road cycling. Mountain bikes share some similarities with other bicycles, but incorporate features designed to enhance durability and performance in rough terrain, which makes them heavy. These typically include a suspension fork, large knobby tires, more durable wheels, more powerful brakes, straight, extra wide handlebars to improve balance and comfort over rough terrain, lower gear-ratios for climbing steep grades and sometimes rear suspension to really smooth out the trail as well as dropper-posts to quickly adjust the seat height.
						</Card.Text>
				    	<Button as={Link} to="/products" variant="warning" style={{color: "brown"}} className="center">Go to product list</Button>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card style={{background: "darkgray"}}>
					<img src={bmx} className="responsive-image"/>
					<Card.Body style={{color: "white"}}>
						<Card.Title style={{textAlign: "center"}}>BMX</Card.Title>
						<Card.Text>
					      BMX is an acronym for Bicycle Motor Cross, primarily because this type of bicycle is a single-speed bike that is raced around short dirt tracks, quite similar in nature to the motor sport. The acronym is also often used describe any bike with single-speed and a 20-inch wheel. This bikes are ideal for people who intend to perform tricks and jumps with their bikes because they are especially built for that, with their robust and durable design and structure. They consist of small frames, a single gear and twenty inch wheels which means that they are not only strong but very low maintenance too as compared to an average bicycle.
						</Card.Text>
				    	<Button as={Link} to="/products" variant="warning" style={{color: "brown"}}>Go to product list</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>

	)
}